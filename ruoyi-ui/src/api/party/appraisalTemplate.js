import request from '@/utils/request'

// 查询模板列表
export function queryList(params) {
	return request({
		url: '/template/list',
		method: 'get',
		params: params
	})
}

// 新增模板
export function addTemplate(params) {
	return request({
		url: '/template/add',
		method: 'post',
		data:  params
	})
}

// 修改模板
export function updateTemplate(params) {
	return request({
		url: '/template/update',
		method: 'put',
		data: params
	})
}

// 删除考评模板
export function delTemplate(id) {
  return request({
    url: '/template/' + id,
    method: 'delete'
  })
}


// 管理指标 -- 点击操作栏 管理指标 -- 查询考评模板下的 所有一级指标 和 二级指标，一个模板对应多个一级指标，一个一级指标对应多个二级
export function getTemplate(id) {
  return request({
    url: '/template/getQuota/'+id, 
    method: 'get'
  })
}

// 新增一级指标
export function addOnelevel(params) {
	return request({
		url: '/onelevel',
		method: 'post',
		data:  params
	})
}

// 修改一级指标
export function updateOnelevel(params) {
	return request({
		url: '/onelevel',
		method: 'put',
		data: params
	})
}

// 删除一级指标 -- 同时会删除一级指标下的所有二级指标
export function deleteOne(id) {
  return request({
    url: '/onelevel/'+id, 
    method: 'delete'
  })
}

// 获取二级指标 -- 根据一级指标Id，获取二级指标      
export function getTwoByOne(id) {
  return request({
    url: '/onelevel/'+id, 
    method: 'get'
  })
}

//添加二级指标
export function addTwo(params) {
  return request({
    url: '/twolevel',
    method: 'post',
    data:  params
  })
}

//修改二级指标
export function updateTwo(params) {
  return request({
    url: '/twolevel',
    method: 'put',
    data:  params
  })
}

 //删除二级指标
 export function delTwo(id) {
  return request({
    url: '/twolevel/' + id,
    method: 'delete'
  })
}

// 获取二级指标详情
 export function getTwo(id) {
  return request({
    url: '/twolevel/'+id, 
    method: 'get'
  })
}

// 排序二级指标
export function updateTwoList(params) {
  return request({
 	url: '/twolevel/order',
    method: 'post',
    data: params
  })
}

// 排序一级指标
export function updateOneList(params) {
  return request({
 url: '/onelevel/order',
    method: 'post',
    data: params // 前端排序好的数组
  })
}

// 复制考评模板
export function copyTemplate(params){
	return request({
		url: '/template/copy',
		method: 'post',
		data: params
	})
}

// 根据模板ID获取详细信息 -- 一级指标里面包含着 二级
export function getTemplateInfo(id) {
  return request({
    url: '/template/'+id, // url 后面 1 是 模板Id
    method: 'get'
  })
}