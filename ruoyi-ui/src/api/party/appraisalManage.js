import request from '@/utils/request'

// 查询考评试卷
export function appraisalManageList(params) {
	return request({
		url: '/app/list',
		method: 'get',
		params: params
	})
}

// 新增考评试卷
export function addTemplate(params) {
	return request({
		url: '/app/add',
		method: 'post',
		data:  params
	})
}

// 修改考评试卷信息
export function updateTemplate(params) {
	return request({
		url: '/app/update',
		method: 'post',
		data:  params
	})
}

// 复制考评试卷信息
export function copyTemplate(params){
	return request({
		url: '/app/copy',
		method: 'post',
		data:  params
	})
}

// 点击标题 -- 查看详情
export function getInfo(id) {
  return request({
    url: '/app/get/'+id, // url 后面  1 是考评试卷Id
    method: 'get'
  })
}

// 删除考评试卷
export function deleteOne(id) {
	return request({
	  url: '/app/'+id,   // 1 为 考评试卷Id 可多个  [1,2,3]
	  method: 'delete'
	})
}

// 图片上传
export function imageUpload(params){
	return request({
		url: '/app/appPicUpload',
		method: 'post',
		data:  params
	})
}
// 截止时间修改
export function updateEndDate(params){
	return request({
		url: '/app/updateEndDate',
		method: 'post',
		data:  params
	})
}