import request from '@/utils/request'
import { praseStrEmpty } from "@/utils/ruoyi";

// 查询党员列表
export function listSysStaff(query) {
  return request({
    url: '/party/user/list',
    method: 'get',
    params: query
  })
}

// 查询党员详细
export function getSysStaff(sysStaffId) {
  return request({
    url: '/party/user/' + praseStrEmpty(sysStaffId),
    method: 'get'
  })
}

// 新增党员
export function addSysStaff(data) {
  return request({
    url: '/party/user',
    method: 'post',
    data: data
  })
}

// 修改党员
export function updateSysStaff(data) {
  return request({
    url: '/party/user',
    method: 'put',
    data: data
  })
}

// 删除党员
export function delSysStaff(sysStaffId) {
  return request({
    url: '/party/user/' + sysStaffId,
    method: 'delete'
  })
}

// 导出党员
export function exportSysStaff(query) {
  return request({
    url: '/party/user/export',
    method: 'get',
    params: query
  })
}



// 下载用户导入模板 暂时不用，后台未做
export function importTemplate() {
  return request({
    url: '/party/user/importTemplate',
    method: 'get'
  })
}
