import request from '@/utils/request'

// 查询组织列表
export function listParty(query) {
  return request({
    url: '/party/organization/list',
    method: 'get',
    params: query
  })
}

// 查询部门详细
export function getParty(partyId) {
  return request({
    url: '/party/organization/' + partyId,
    method: 'get'
  })
}

// 查询部门下拉树结构
export function treeselect() {
  return request({
    url: '/party/organization/treeselect',
    method: 'get'
  })
}

// 根据角色ID查询部门树结构
export function rolePartyTreeselect(roleId) {
  return request({
    url: '/system/dept/roleDeptTreeselect/' + roleId,
    method: 'get'
  })
}

// 新增部门
export function addParty(data) {
  return request({
    url: '/party/organization',
    method: 'post',
    data: data
  })
}

// 修改部门
export function updateParty(data) {
  return request({
    url: '/party/organization',
    method: 'put',
    data: data
  })
}

// 删除部门
export function delParty(partyId) {
  return request({
    url: '/party/organization/' + partyId,
    method: 'delete'
  })
}