import request from '@/utils/request'

// 分页查询
export function queryList(params) {
	return request({
		url: '/new/list',
			method: 'get',
			params: params
	})
}

// 新增党建要闻
export function addPartyNews(params) {
  return request({
    url: '/new/add',
    method: 'post',
    data:  params
  })
}

// 修改党建要闻
export function updatePartyNews(params) {
  return request({
    url: '/new/edit',
    method: 'post',
    data:  params
  })
}

// 图片上传
export function imageUpload(params){
	return request({
		url: '/new/uploadPicture',
		method: 'post',
		data:  params
	})
}

// 附件上传
export function attchUpload(params){
	return request({
		url: '/attch/upload',
		method: 'post',
		data:  params
	})
}

// 删除党建要闻数据
export function deletePartyNews(ids) {
  return request({
    url: '/new/'+ids,   // 7 为 党建要闻Id, 可多个  [7,8,9]
		method: 'delete',
  })
}

// 获取详情
export function getPartyNews(id){
	return request({
		url: '/new/get/'+id,
		method: 'get',
	})
}

// 设置首页推荐
export function updateHomePage(id){
	return request({
	 url: '/new/updateHomePage/'+id, // url -- 7 主题活动Id
	 method: 'post'
	})
}