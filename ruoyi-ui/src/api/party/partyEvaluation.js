import request from '@/utils/request'

// 分页查询
export function queryList(params) {
	return request({
		url: '/partyApp/list',
			method: 'get',
			params: params
	})
}

// 根据考评试卷Id - 获取考评详情信息，主要是人员
export function getInfo(id) {
  return request({
    url: '/partyApp/'+id, // url 后面  1 是考评试卷Id
    method: 'get'
  })
}

// 选择某人 去评分 根据考评模板ID-- 一级指标里面包含着 二级指标  二级里面包含着 选项信息  
export function getUserInfo(id) {
  return request({
    url: '/partyApp/Template/'+id, // url 后面  1 是考评模板Id
    method: 'get'
  })
}

// 提交考评
export function submitAppraisal(data) {
  return request({
    url: '/app/add',
    method: 'post',
    data: data,
  //   data:  {
	// 	appraisaMainId: null,
	// 	appraisaID: '考评试卷Id',
	// 	appraisaObjectID: '', // 考评对象Id
	// 	subDate: Date, // 考评时间 
	// 	appraisaDetails: [{appraisaDetailID: null, appraisaMainId: '考评记录主表 Id', appraisaOptionID: '考评选项表 Id', appraisaOptionName: '考评选项名称',
	// 					  appraisaOptionScore:'考评选项分值', appraisaOptionOrder: '考评选项排序' , appraisalTemplateID: '考评模板ID' }..] // 多个选项scoreTotal
		
	// }
  })
}


// 选择某人 去评分 根据考评模板ID-- 一级指标里面包含着 二级指标  二级里面包含着 选项信息  
export function evaluate(id) {
  return request({
    url: '/partyApp/Template/'+id, // url 后面  1 是考评模板Id
    method: 'get'
  })
}