import request from '@/utils/request'
import { praseStrEmpty } from "@/utils/ruoyi";


// 测试接口所用
//export function getInfo() {
//  return request({
//    url: '/app/downImg/6',
//	method: 'get',
//  })
//}
 
 
// 显示图片 
export function getImg(id) {
	return request({
		url: '/app/downImg/' + id,
		method: 'get',
	})
}


// 查询会议列表
export function listMett(query) {
  return request({
    url: '/metting/list',
    method: 'get',
    params: query
  })
}

// 查询会议详细
export function getMett(mettId) {
  return request({
    url: '/metting/' + praseStrEmpty(mettId),
    method: 'get'
  })
}

// 新增会议
export function addMett(data) {
  return request({
    url: '/metting',
    method: 'post',
    data: data
  })
}

// 修改会议
export function updateMett(data) {
  return request({
    url: '/metting',
    method: 'put',
    data: data
  })
}

// 删除会议
export function delMett(mettIds) {
  return request({
    url: '/metting/' + mettIds,
    method: 'delete'
  })
}

// 会议下载
export function downloadFile(attachmentId) {
  return request({
    url: '/attch/downloadFile/' + attachmentId,
    method: 'get'
  })
}



// 删除附件
export function deleteFile(attachmentId) {
  return request({
    url: '/attch/' + attachmentId,
    method: 'delete'
  })
}

