import request from '@/utils/request'

// 查询主题活动
export function getList(params) {
	return request({
		url: '/theme/list',
		method: 'get',
		params: params
	})
}

// 新增主题活动
export function addList(params) {
	return request({
		url: '/theme/add',
		method: 'post',
		data:  params
	})
}

// 修改主题活动
export function editList(params) {
	return request({
		url: '/theme/edit',
		method: 'post',
		data:  params
	})
}

// 删除主题活动
export function deleteList(id) {
	return request({
	  url: '/theme/'+id,   // 1 为 活动主题Id 可多个  1,2,3
	  method: 'delete'
	})
}

//列表查询 
export function getType(){
	return request({
		url: '/theme/type/list',
		method: 'get'
	})
	
}

//  点击主题活动 --  显示详情信息 -- 人员(都在一起，前端需要处理数据，取出)， 附件信息
export function getActivities(id) {
	return request({
	  url: '/theme/get/'+id, // url 后面 1 活动主题Id
	  method: 'get'
	})
}

// 新增分类
export function addType(params) {
  return request({
    url: '/theme/type',
    method: 'post',
    data: params
  })
}

// 修改分类
export function editType(params) {
  return request({
    url: '/theme/type',
    method: 'put',
    data: params
  })
}

// 删除分类
export function deleteType(id) {
  return request({
    url: '/theme/type/'+id,   // 1 为 分类Id 可多个  1,2,3
    method: 'delete'
  })
}

// 活动记录查询
// export function getRecord(){
// 	return request({
// 		url: '/record/list',
// 		method: 'get'
// 	})
// }

// 新增活动记录
export function addRecord(params){
	return request({
		url: '/record/add',
		method: 'post',
		data: params
	})
}

// 修改活动记录
export function editRecord(params){
	return request({
		url: '/record/edit',
		method: 'post',
		data: params
	})
}

// 获取详情
export function getInfoRecord(id){
	return request({
		url: '/record/get/'+id, // url 后面 1 是 活动主题Id
		method: 'get'
	})
}

// 删除管理
export function deleteRecord(id){
	return request({
		url: '/record/'+id, // url 后面 1 是 活动记录 Id
		method: 'delete'
	})
}

//人员搜索
export function getPersons(params){
	return request({
		url: '/theme/listPerson/',
		method: 'post',
		data: {
		personType: '3',
		ids: [100] // 需要搜索的人员Id集合，
		}
	})
}

// 设置首页推荐
export function updateHomePage(id){
	return request({
	 url: '/theme/updateHomePage/'+id, // url -- 7 主题活动Id
	 method: 'post'
	})
}

//取消推荐
export function cancelRecommended(id) {
  return request({
    url: '/theme/updateCancelHomePage/'+id, // 1 活动主题Id
    method: 'post'
  })
}

// 取消
export function cancelTheme(id) {
  return request({
    url: '/theme/cancelTheme/'+id, // url  1  是活动主题Id 
    method: 'post'
  })
}